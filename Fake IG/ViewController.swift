//
//  ViewController.swift
//  Fake IG
//
//  Created by M3 Beatz on 3/26/21.
//

import UIKit
import AVFoundation
import StoreKit

enum AppStoreReviewManager {
    static func requestReviewIfAppropriate() {
        SKStoreReviewController.requestReview()
    }
}

class ViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    var usernamesList = [String]()
    var lastFiveUsers = [String]()
    var commentsList = [String]()
    var lastFiveComments = [String]()
    var pfpList = [UIImage]()
    var lastFivePfp = [Int]()
    var runCount: Int! {
        set {
            defaults.setValue(newValue, forKey: "runCount")
        }
        get {
            if defaults.object(forKey: "runCount") == nil {
                defaults.setValue(0, forKey: "runCount")
            }
            return defaults.integer(forKey: "runCount")
        }
    }
    var ratingTime: TimeInterval! {
        set {
            defaults.setValue(newValue, forKey: "ratingTime")
        }
        get {
            if defaults.object(forKey: "ratingTime") == nil {
                defaults.setValue(0, forKey: "ratingTime")
            }
            return defaults.double(forKey: "ratingTime") as TimeInterval
        }
    }
    
    var username: String! {
        set {
            if newValue == nil || newValue == "" {
                defaults.setValue("user", forKey: "username")
                return
            }
            defaults.setValue(newValue.lowercased(), forKey: "username")
        }
        
        get {
            if defaults.object(forKey: "username") == nil {
                defaults.setValue("user", forKey: "username")
            }
            return defaults.string(forKey: "username")
        }
    }
    
    var pfpImg: UIImage! {
        set {
            if newValue == nil {
                defaults.setValue(UIImage(named: "defaultPfp.jpeg")!.pngData(), forKey: "pfpImg")
                return
            }
            defaults.setValue(newValue.pngData(), forKey: "pfpImg")
        }
        get {
            if defaults.object(forKey: "pfpImg") == nil {
                defaults.setValue(UIImage(named: "defaultPfp.jpeg")!.pngData(), forKey: "pfpImg")
            }
            return UIImage(data: defaults.data(forKey: "pfpImg")!)
        }
    }
    
    var isDefaultPfp: Bool! {
        set {
            defaults.setValue(newValue, forKey: "defaultPfp")
        }
        get {
            if defaults.object(forKey: "defaultPfp") == nil {
                defaults.setValue(true, forKey: "defaultPfp")
            }
            return defaults.bool(forKey: "defaultPfp")
        }
    }
    
    var autoViewers: Bool! {
        set {
            defaults.setValue(newValue, forKey: "autoViewers")
        }
        get {
            if defaults.object(forKey: "autoViewers") == nil {
                defaults.setValue(false, forKey: "autoViewers")
            }
            return defaults.bool(forKey: "autoViewers")
        }
    }
    
    var viewerTimer: Timer!
    
    var cameraView: UIView!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var rotateButton: UIButton!
    var closeButton: UIButton!
    
    var captureSession: AVCaptureSession!
    
    var frontCamera: AVCaptureDevice!
    var backCamera: AVCaptureDevice!
    var frontBack: Bool!
    
    var frontInput: AVCaptureInput!
    var backInput: AVCaptureInput!
    
    var usernameField: UITextField!
    let usernameAttr = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedString.Key.foregroundColor : UIColor.white]
    
    var arrow: UIImageView!
    var arrowConst: NSLayoutConstraint!
    
    var viewerConst: NSLayoutConstraint!
    var viewerWidthConst: NSLayoutConstraint!
    var eyeTrailConst: NSLayoutConstraint!
    
    var viewerView: UIView!
    var viewerLabel: UILabel!
    var viewerAlert: UIAlertController!
    var viewerCount: Int! {
        set {
            defaults.setValue(newValue, forKey: "viewerCount")
        }
        get {
            if defaults.object(forKey: "viewerCount") == nil {
                defaults.setValue(0, forKey: "viewerCount")
            }
            return defaults.integer(forKey: "viewerCount")
        }
    }
    
    var eyeImg: UIImageView!
    var pfp: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    var comments = [Comment]()
    var tableView: UITableView!
    
    var noCameraView: UILabel!
    
    var lastViewerVal: Int!
    
    var tableViewGradient: CAGradientLayer!
    
    var coverView: UIView!
    
    var firstRun: Bool! {
        set {
            defaults.setValue(newValue, forKey: "firstRun")
        }
        get {
            if defaults.object(forKey: "firstRun") == nil {
                defaults.setValue(true, forKey: "firstRun")
            }
            return defaults.bool(forKey: "firstRun")
        }
    }
    
    var usernameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).viewController = self
        runCount += 1
        view.backgroundColor = .black
        view.alpha = firstRun ? 0 : 1
        loadData()
        setupViews()
        setupInputs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkPermissions() {
            
            self.onboarding() {
                self.addTapRecognizers()
                self.startViewers()
                self.startComments()
                self.ratingAction()
                self.startCapture()
            }
            
            UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut) {
                self.view.alpha = 1
            }
        }
    }
    
    var picTap: UITapGestureRecognizer!
    var viewerTap: UITapGestureRecognizer!
    var dismissTap: UITapGestureRecognizer!
    
    func addTapRecognizers() {
        view.addGestureRecognizer(flipTap)
        pfp.addGestureRecognizer(picTap)
        viewerView.addGestureRecognizer(viewerTap)
    }
    
    var ratingTimer: Timer!
    
    func ratingAction() {
        
        if ratingTime >= 420 {
            AppStoreReviewManager.requestReviewIfAppropriate()
            self.ratingTime = 0
        }
        
        if ratingTimer != nil {
            ratingTimer.invalidate()
            ratingTimer = nil
        }
        
        ratingTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.ratingTime += 1
        })
        
    }
    var editingUsername = false
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if tableView != nil && tableViewGradient != nil {
            tableViewGradient.frame = CGRect(origin: CGPoint(x: 0, y: self.coverView.frame.maxY - 300), size: CGSize(width: self.coverView.frame.width - 80, height: 256))
        }
        if usernameLabel != nil {
            if !editingUsername {
                usernameField.frame = CGRect(origin: usernameLabel.frame.origin, size: CGSize(width: usernameLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: usernameLabel.frame.height)).width, height: usernameLabel.frame.height))
            }
        }
        guard maskLight != nil, maskLightView != nil else {
            return
        }
        
//        maskLight.frame = maskLightView.frame
    }
    
    var maskLightView: UIView!
    
    func onboarding(_ completion: @escaping () -> Void) {
        
        if firstRun {
            self.onboardAction()
        } else {
            completion()
        }
    }
    
    var blurView: UIVisualEffectView!
    
    @objc func onboardAction() {
        if editingUsername || showingHelp { return }
        showingHelp = true
        closeButton.isUserInteractionEnabled = false
        if commentTimer != nil {
            commentTimer.invalidate()
            commentTimer = nil
        }
        if viewerTimer != nil {
            viewerTimer.invalidate()
            viewerTimer = nil
        }
        view.removeGestureRecognizer(flipTap)
        view.removeGestureRecognizer(dismissTap)
        pfp.removeGestureRecognizer(picTap)
        
        blurView = UIVisualEffectView(effect: UIBlurEffect(style: self.firstRun ? .light : .regular))
        blurView.frame = view.frame
        blurView.backgroundColor = .clear
        blurView.alpha = 0
        blurView.tag = -1
        blurView.isUserInteractionEnabled = !firstRun
        view.addSubview(blurView)
        
        maskLightView = UIView()
        maskLightView.backgroundColor = .clear
        maskLightView.frame = self.view.frame
        maskLightView.alpha = 0
        self.view.insertSubview(maskLightView, aboveSubview: cameraView)
        
        maskLight = CAGradientLayer()
        maskLight.masksToBounds = true
        maskLight.colors = [UIColor(red: 204/255, green: 0, blue: 141/255, alpha: 1).cgColor, UIColor(red: 233/255, green: 0/255, blue: 52/255, alpha: 1).cgColor]
        maskLight.startPoint = CGPoint(x: 0.0, y: 0.0)
        maskLight.endPoint = CGPoint(x: 1.0, y: 1.0)
        maskLightView.layer.addSublayer(maskLight)
                
        let testlabel = UITextView(frame: CGRect(origin: CGPoint(x: 16, y: view.center.y - 192), size: CGSize(width: blurView.frame.width - 32, height: 240)))
        testlabel.isUserInteractionEnabled = false
        testlabel.attributedText = NSAttributedString(string: "Go Livë", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.white])
        testlabel.backgroundColor = .clear
        testlabel.textAlignment = .center
        testlabel.tag = 1
        blurView.contentView.addSubview(testlabel)
        
        let path = UIBezierPath(rect: blurView.bounds)
        
        let mask = CAShapeLayer()
        mask.fillRule = .evenOdd
        mask.path = path.cgPath
        mask.fillColor = UIColor.clear.cgColor
        blurView.layer.mask = mask
        
        if !firstRun {
            self.maskLight.frame = self.view.convert(CGRect(origin: CGPoint(x: self.pfp.frame.origin.x - 8, y: self.pfp.frame.origin.y - 8), size: CGSize(width: self.pfp.frame.size.width + 16, height: self.pfp.frame.size.height + 16)), from: self.pfp.superview!)
            self.maskLight.cornerRadius = self.pfp.layer.cornerRadius
        } else {
            self.maskLight.frame = self.view.frame
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            self.blurView.alpha = 1
            self.maskLightView.alpha = self.firstRun ? 1 : 0
        } completion: { (done) in
            if self.captureSession != nil {
                self.previewLayer.removeFromSuperlayer()
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
                testlabel.alpha = 1
                mask.fillColor = UIColor.black.cgColor
            } completion: { (complete) in
                self.priorComments = self.comments
                self.comments = [Comment(.commented, 0, self), Comment(.commented, 0, self), Comment(.joinMultiple, 3, self), Comment(.joinSingle, 0, self)]
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                    self.tableView.reloadData()
                })
                self.blurView.isUserInteractionEnabled = true
            }
        }

        
        let btn = UIButton()
        btn.frame = CGRect(origin: CGPoint(x: 24, y: blurView.frame.maxY - 128), size: CGSize(width: blurView.frame.width - 48, height: 48))
        btn.backgroundColor = UIColor.white
        btn.layer.cornerRadius = 4
        if firstRun {
            btn.setAttributedTitle(NSAttributedString(string: "Go", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.black]), for: .normal)
        } else {
            btn.setAttributedTitle(NSAttributedString(string: "Next", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.black]), for: .normal)
        }
        
        btn.addTarget(self, action: #selector(advance(_:)), for: .touchUpInside)
        blurView.contentView.addSubview(btn)
        
        if !firstRun {
            self.advance(btn)
        }
        
        closeButton.isUserInteractionEnabled = true
        
    }
    
    var maskLight: CAGradientLayer!
    var priorComments = [Comment]()
    var showingHelp = false
    
    @objc func advance(_ btn: UIButton) {
//        btn.isUserInteractionEnabled = false
        
        guard let mask = blurView.layer.mask as? CAShapeLayer, let testlabel = blurView.contentView.viewWithTag(1) as? UITextView else {
            print("No mask or label")
//            btn.isUserInteractionEnabled = true
            showingHelp = false
            return
        }
        
        if blurView.tag <= 4 {
            mask.removeFromSuperlayer()
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            testlabel.alpha = (!self.firstRun && self.blurView.tag == -1) ? 1 : 0
            if self.blurView.tag > 4 {
                btn.alpha = 0
            }
        } completion: { (complete) in
            var path: UIBezierPath!
            
            if self.blurView.tag == -1 {
                path = UIBezierPath(roundedRect: self.pfp.frame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: self.pfp.layer.cornerRadius, height: self.pfp.layer.cornerRadius))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = self.view.convert(CGRect(origin: CGPoint(x: self.pfp.frame.origin.x - 8, y: self.pfp.frame.origin.y - 8), size: CGSize(width: self.pfp.frame.size.width + 16, height: self.pfp.frame.size.height + 16)), from: self.pfp.superview!)
                self.maskLight.cornerRadius = self.pfp.layer.cornerRadius
                                
                btn.setAttributedTitle(NSAttributedString(string: "Next", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.black]), for: .normal)
                
                let myString = NSMutableAttributedString(string: "Profile Picture\n\nTap to change/remove image")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .semibold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 15))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 15, length: myString.string.count - 15))

                testlabel.attributedText = myString
                
                self.blurView.tag += 1
            } else if self.blurView.tag == 0 {
                path = UIBezierPath(roundedRect: self.usernameField.frame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 4, height: 4))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = CGRect(origin: CGPoint(x: self.usernameField.frame.origin.x - 8, y: self.usernameField.frame.origin.y - 8), size: CGSize(width: self.usernameField.frame.size.width + 16, height: self.usernameField.frame.size.height + 16))
                self.maskLight.cornerRadius = 4
                
                let myString = NSMutableAttributedString(string: "Username\n\nTap to edit")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 8))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 8, length: myString.string.count - 8))
                
                testlabel.attributedText = myString
                
                self.blurView.tag += 1
            } else if self.blurView.tag == 1 {
                path = UIBezierPath(roundedRect: self.viewerView.frame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 4, height: 4))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = CGRect(origin: CGPoint(x: self.viewerView.frame.origin.x - 8, y: self.viewerView.frame.origin.y - 8), size: CGSize(width: self.viewerView.frame.size.width + 16, height: self.viewerView.frame.size.height + 16))
                self.maskLight.cornerRadius = 4
                
                let myString = NSMutableAttributedString(string: "Number of Viewers\n\nTap to edit\nFlux Mode: Viewers change\nStatic Mode: Viewers remain still")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 17))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 17, length: myString.string.count - 17))
                
                testlabel.attributedText = myString
                
                self.blurView.tag += 1
            } else if self.blurView.tag == 2 {
                
                let tableViewFrame = CGRect(origin: CGPoint(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y + 80), size: CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height - 88))
                
                path = UIBezierPath(roundedRect: tableViewFrame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 4, height: 4))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = CGRect(origin: CGPoint(x: tableViewFrame.origin.x - 8, y: tableViewFrame.origin.y - 8), size: CGSize(width: tableViewFrame.size.width + 16, height: tableViewFrame.size.height + 16))
                self.maskLight.cornerRadius = 4
                
                let myString = NSMutableAttributedString(string: "Comment Section\n\nInteract by scrolling\nComments begin when there are more than 0 viewers")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 15))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 15, length: myString.string.count - 15))
                
                testlabel.attributedText = myString
                
                btn.setAttributedTitle(NSAttributedString(string: "Next", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.black]), for: .normal)
                
                self.blurView.tag += 1
            } else if self.blurView.tag == 3 {
                path = UIBezierPath(roundedRect: self.rotateButton.frame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 4, height: 4))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = CGRect(origin: CGPoint(x: self.rotateButton.frame.origin.x - 8, y: self.rotateButton.frame.origin.y - 8), size: CGSize(width: self.rotateButton.frame.size.width + 16, height: self.rotateButton.frame.size.height + 16))
                self.maskLight.cornerRadius = 4
                
                let myString = NSMutableAttributedString(string: "Rotate Button\n\nTap to flip camera, or double tap screen")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 13))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 13, length: myString.string.count - 13))
                
                testlabel.attributedText = myString
                
                self.blurView.tag += 1
            } else if self.blurView.tag == 4 {
                path = UIBezierPath(roundedRect: self.closeButton.frame, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 4, height: 4))
                path.append(UIBezierPath(rect: self.blurView.bounds))
                
                self.maskLight.frame = CGRect(origin: CGPoint(x: self.closeButton.frame.origin.x - 8, y: self.closeButton.frame.origin.y - 8), size: CGSize(width: self.closeButton.frame.size.width + 16, height: self.closeButton.frame.size.height + 16))
                self.maskLight.cornerRadius = 4
                
                let myString = NSMutableAttributedString(string: "Close Button\n\nTap to display this information again")
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .bold).withTraits(.traitItalic), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 0, length: 12))
                myString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23, weight: .light), NSAttributedString.Key.foregroundColor : UIColor.white], range: NSRange(location: 12, length: myString.string.count - 12))
                
                testlabel.attributedText = myString
                
                btn.setAttributedTitle(NSAttributedString(string: "Done", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.black]), for: .normal)
                
                self.blurView.tag += 1
            } else {
                self.comments = self.priorComments
                self.priorComments = []
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                self.startCapture()
                
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
                    self.maskLightView.alpha = 0
                }
                
                UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut) {
                    self.blurView.alpha = 0
                    self.usernameField.backgroundColor = .clear
                    self.closeButton.backgroundColor = .clear
                    self.tableView.backgroundColor = .clear
                } completion: { (done) in
                    self.blurView.tag = 0
                    self.blurView.removeFromSuperview()
                    self.showingHelp = false
                    self.maskLightView.removeFromSuperview()
                    self.firstRun = false
                    self.addTapRecognizers()
                    self.startViewers()
                    self.startComments()
                }
                return
            }
            
            testlabel.textAlignment = .center
            
            mask.path = path.cgPath
            self.blurView.layer.mask = mask
            
            if self.blurView.tag == 0 && self.firstRun {
                self.blurView.effect = UIBlurEffect(style: .regular)
                if !self.firstRun {
                    self.view.layoutIfNeeded()
                }
            }
            
            UIView.animate(withDuration: 0.3, delay: self.blurView.tag == 0 ? 0.3 : 0, options: .curveEaseOut) {
                testlabel.alpha = 1
                self.maskLightView.alpha = 1
            } completion: { (complete) in
//                btn.isUserInteractionEnabled = true
            }

        }

    }
    
    func loadData() {
        if let filepath = Bundle.main.path(forResource: "usernames", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                self.usernamesList = contents.components(separatedBy: "\n").filter({ (s) -> Bool in
                    return s != "" && s != "\n" && s != "\r" && s != " "
                })
            } catch {
                print("Couldn't load usernames")
            }
        } else {
            print("usernames.txt not found")
        }
        
        if let filepath = Bundle.main.path(forResource: "comments", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                self.commentsList = contents.components(separatedBy: "\n")
                self.commentsList = self.commentsList.filter { (s) -> Bool in
                    return s != "" && s != "\n" && s != "\r" && s != " "
                }
            } catch {
                print("Couldn't load comments")
            }
        } else {
            print("comments.txt not found")
        }
        
        for i in 0...20 {
            pfpList.append(UIImage(named: "pfp_\(i).png")!)
        }
    }

    var commentTimer: Timer!
    
    func startComments() {
        if commentTimer != nil {
            commentTimer.invalidate()
            commentTimer = nil
        }
        
        if self.viewerCount == 0 {
            if self.comments.isEmpty { return }
            var paths = [IndexPath]()
            for i in 0..<self.comments.count {
                paths.append(IndexPath(row: i, section: 0))
            }
            self.comments.removeAll()
            self.tableView.deleteRows(at: paths, with: .top)
            return
        }
                
        var timing = 0.7
        var repeats: UInt32 = 0
        
        // 0-9
        if self.viewerCount <= 9 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/9.0))+33))/10.0)
            repeats = 0
        } else
        
        // 10-99
        if self.viewerCount <= 99 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/99.0))+31))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 0 : 1
        } else
        
        // 100-999
        if self.viewerCount <= 999 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/999.0))+27))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 1 : 2
        } else
        
        // 1k-9k
        if self.viewerCount <= 9999 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/9999.0))+23))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 1 : 2
        } else
        
        // 10k-99k
        if self.viewerCount <= 99999 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/99999.0))+21))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 2 : 3
        } else
        
        // 100k-999k
        if self.viewerCount <= 999999 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/999999.0))+17))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 2 : 3
        } else
        
        // 1m-9.9m
        if self.viewerCount <= 9999999 {
            timing = TimeInterval(Double(arc4random_uniform(21 - UInt32(21*(Float(self.viewerCount)/9999999.0))+12))/10.0)
            repeats = arc4random_uniform(3) != 0 ? 3 : 4
        }
                
        commentTimer = Timer.scheduledTimer(withTimeInterval: timing, repeats: false, block: { (timer) in
            var insertedRows = [IndexPath]()
            let once = (self.comments.isEmpty || arc4random_uniform(3) != 0)
            let endRange = Int(once ? 1 : (arc4random_uniform(repeats)+1))
            for i in 0..<endRange {
                var type: CommentType!
                
                let rand = self.comments.count <= 3 ? arc4random_uniform(1) + 5 : arc4random_uniform(7)

                switch rand {
                case 0,1,2,3:
                    type = .commented
                case 4,5:
                    type = .joinSingle
                case 6:
                    type = .joinMultiple
                default:
                    type = .commented
                }
                
                if self.comments.count == 2 && !self.comments.contains(where: { (c) -> Bool in
                    return c.type == .joinMultiple
                }) {
                    type = .joinMultiple
                }
                
                var maxOthers: UInt32 = 3
                
                if self.viewerCount < 20 {
                    maxOthers = 0
                } else if self.viewerCount < 100 {
                    maxOthers = 7
                } else if self.viewerCount < 1000 {
                    maxOthers = 12
                } else {
                    maxOthers = 88
                }
                
                let newComment = Comment(type, Int(arc4random_uniform(maxOthers) + 1), self)
                self.comments.insert(newComment, at: 0)
                insertedRows.append(IndexPath(row: i, section: 0))
                
            }
            
            self.tableView.insertRows(at: insertedRows, with: .top)
            self.startComments()
        })
        
    }
    

    var flipTap: UITapGestureRecognizer!
    
    func setupViews() {
        cameraView = UIView()
        cameraView.translatesAutoresizingMaskIntoConstraints = false
        cameraView.backgroundColor = .black
        cameraView.layer.cornerRadius = 8
        cameraView.layer.masksToBounds = true
        view.addSubview(cameraView)
        cameraView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        cameraView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        cameraView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        cameraView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -82).isActive = true
        
        flipTap = UITapGestureRecognizer(target: self, action: #selector(flipCamera))
        flipTap.numberOfTouchesRequired = 1
        flipTap.numberOfTapsRequired = 2
        flipTap.delegate = self
                
        pfp = UIImageView()
        pfp.translatesAutoresizingMaskIntoConstraints = false
        pfp.contentMode = .scaleAspectFill
        pfp.image = pfpImg
        pfp.backgroundColor = .clear
        pfp.layer.cornerRadius = 16
        pfp.layer.masksToBounds = true
        pfp.isUserInteractionEnabled = true
        view.addSubview(pfp)
        pfp.topAnchor.constraint(equalTo: cameraView.topAnchor, constant: 12).isActive = true
        pfp.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor, constant: 16).isActive = true
        pfp.widthAnchor.constraint(equalToConstant: 32).isActive = true
        pfp.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        picTap = UITapGestureRecognizer(target: self, action: #selector(changePic))
        picTap.numberOfTouchesRequired = 1
        picTap.numberOfTapsRequired = 1
        
        closeButton = UIButton()
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.contentHorizontalAlignment = .fill
        closeButton.contentVerticalAlignment = .fill
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.tintColor = .white
        closeButton.backgroundColor = .clear
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(onboardAction), for: .touchUpInside)
        view.addSubview(closeButton)
        closeButton.centerYAnchor.constraint(equalTo: pfp.centerYAnchor).isActive = true
        closeButton.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor, constant: -12).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        let fakePicButton = UIButton()
        fakePicButton.setImage(UIImage(named: "fakeImg.png"), for: .normal)
        fakePicButton.contentHorizontalAlignment = .fill
        fakePicButton.contentVerticalAlignment = .fill
        fakePicButton.imageView?.contentMode = .scaleAspectFit
        fakePicButton.tintColor = .white
        fakePicButton.backgroundColor = .clear
        fakePicButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(fakePicButton)
        fakePicButton.centerXAnchor.constraint(equalTo: closeButton.centerXAnchor).isActive = true
        fakePicButton.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 26).isActive = true
        fakePicButton.widthAnchor.constraint(equalToConstant: 26).isActive = true
        fakePicButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        rotateButton = UIButton()
        rotateButton.setImage(UIImage(named: "rotate.png"), for: .normal)
        rotateButton.contentHorizontalAlignment = .fill
        rotateButton.contentVerticalAlignment = .fill
        rotateButton.imageView?.contentMode = .scaleAspectFit
        rotateButton.tintColor = .white
        rotateButton.backgroundColor = .clear
        rotateButton.translatesAutoresizingMaskIntoConstraints = false
        rotateButton.addTarget(self, action: #selector(flipCamera), for: .touchUpInside)
        view.addSubview(rotateButton)
        rotateButton.centerXAnchor.constraint(equalTo: closeButton.centerXAnchor).isActive = true
        rotateButton.topAnchor.constraint(equalTo: fakePicButton.bottomAnchor, constant: 24).isActive = true
        rotateButton.widthAnchor.constraint(equalToConstant: 26).isActive = true
        rotateButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        let fakeEmojiButton = UIButton()
        fakeEmojiButton.setImage(UIImage(named: "emoji.png"), for: .normal)
        fakeEmojiButton.contentHorizontalAlignment = .fill
        fakeEmojiButton.contentVerticalAlignment = .fill
        fakeEmojiButton.imageView?.contentMode = .scaleAspectFit
        fakeEmojiButton.tintColor = .white
        fakeEmojiButton.backgroundColor = .clear
        fakeEmojiButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(fakeEmojiButton)
        fakeEmojiButton.centerXAnchor.constraint(equalTo: closeButton.centerXAnchor).isActive = true
        fakeEmojiButton.topAnchor.constraint(equalTo: rotateButton.bottomAnchor, constant: 24).isActive = true
        fakeEmojiButton.widthAnchor.constraint(equalToConstant: 26).isActive = true
        fakeEmojiButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        viewerView = UIView()
        viewerView.backgroundColor = UIColor.systemGray.withAlphaComponent(0.7)
        viewerView.translatesAutoresizingMaskIntoConstraints = false
        viewerView.layer.cornerRadius = 4
        viewerView.layer.masksToBounds = true
        view.addSubview(viewerView)
        viewerTap = UITapGestureRecognizer(target: self, action: #selector(changeViewers))
        viewerTap.numberOfTouchesRequired = 1
        viewerTap.numberOfTapsRequired = 1
        
        viewerView.trailingAnchor.constraint(equalTo: closeButton.leadingAnchor, constant: -8).isActive = true
        viewerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        viewerView.centerYAnchor.constraint(equalTo: pfp.centerYAnchor).isActive = true
        
        lastViewerVal = viewerCount
        
        viewerLabel = UILabel()
        viewerLabel.backgroundColor = .clear
        viewerLabel.textAlignment = .right
        viewerLabel.layer.cornerRadius = 4
        viewerLabel.layer.masksToBounds = true
        viewerLabel.attributedText = NSAttributedString(string: formatViewerNumber(viewerCount), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.black])
        viewerLabel.translatesAutoresizingMaskIntoConstraints = false
        viewerView.addSubview(viewerLabel)
                
        let viewerTextFrame = viewerLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 30))

        viewerLabel.trailingAnchor.constraint(equalTo: viewerView.trailingAnchor, constant: -8).isActive = true
        viewerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        viewerLabel.centerYAnchor.constraint(equalTo: pfp.centerYAnchor).isActive = true
        viewerConst = viewerLabel.widthAnchor.constraint(equalToConstant: viewerTextFrame.width + 16)
        viewerConst.isActive = true
        
        viewerWidthConst = viewerView.widthAnchor.constraint(equalTo: viewerLabel.widthAnchor, constant: 16)
        viewerWidthConst.isActive = true
                
        eyeImg = UIImageView(image: UIImage(named: "eye.png")!.withTintColor(.black))
        eyeImg.tintColor = .black
        eyeImg.contentMode = .scaleAspectFill
        eyeImg.translatesAutoresizingMaskIntoConstraints = false
        viewerView.addSubview(eyeImg)
        
        eyeImg.widthAnchor.constraint(equalToConstant: 8).isActive = true
        eyeImg.heightAnchor.constraint(equalToConstant: 8).isActive = true
        eyeTrailConst = eyeImg.trailingAnchor.constraint(equalTo: viewerLabel.leadingAnchor, constant: 10)
        eyeTrailConst.isActive = true
        eyeImg.centerYAnchor.constraint(equalTo: viewerView.centerYAnchor).isActive = true
        
        let liveLabel = UILabel()
        liveLabel.backgroundColor = .clear
        liveLabel.textAlignment = .center
        liveLabel.layer.cornerRadius = 4
        liveLabel.layer.masksToBounds = true
        liveLabel.attributedText = NSAttributedString(string: "LIVE", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: .semibold), NSAttributedString.Key.foregroundColor : UIColor.white])
        liveLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(liveLabel)
        
        let liveTextFrame = liveLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 30))
        
        liveLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        liveLabel.widthAnchor.constraint(equalToConstant: liveTextFrame.width + (30 - liveTextFrame.height) ).isActive = true
        liveLabel.centerYAnchor.constraint(equalTo: pfp.centerYAnchor).isActive = true
        liveLabel.trailingAnchor.constraint(equalTo: viewerView.leadingAnchor, constant: -8).isActive = true
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        let liveBG = UIView(frame: liveLabel.frame)
        liveBG.backgroundColor = .clear
        liveBG.layer.cornerRadius = 4
        liveBG.layer.masksToBounds = true
        liveBG.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(liveBG, belowSubview: liveLabel)
        
        liveBG.centerXAnchor.constraint(equalTo: liveLabel.centerXAnchor).isActive = true
        liveBG.centerYAnchor.constraint(equalTo: liveLabel.centerYAnchor).isActive = true
        liveBG.widthAnchor.constraint(equalTo: liveLabel.widthAnchor).isActive = true
        liveBG.heightAnchor.constraint(equalTo: liveLabel.heightAnchor).isActive = true
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(red: 204/255, green: 0, blue: 141/255, alpha: 1).cgColor, UIColor(red: 233/255, green: 0/255, blue: 52/255, alpha: 1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.frame = liveLabel.bounds
        liveBG.layer.insertSublayer(gradientLayer, at: 0)
        
        usernameLabel = UILabel()
        usernameLabel.attributedText = NSAttributedString(string: username, attributes: usernameAttr)
        usernameLabel.backgroundColor = .clear
        usernameLabel.textAlignment = .left
        usernameLabel.alpha = 0
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(usernameLabel)
        
        usernameLabel.topAnchor.constraint(equalTo: pfp.topAnchor).isActive = true
        usernameLabel.leadingAnchor.constraint(equalTo: pfp.trailingAnchor, constant: 8).isActive = true
        usernameLabel.trailingAnchor.constraint(equalTo: liveLabel.leadingAnchor).isActive = true
        usernameLabel.bottomAnchor.constraint(equalTo: pfp.bottomAnchor).isActive = true
        
        usernameField = UITextField()
        usernameField.delegate = self
        usernameField.defaultTextAttributes = usernameAttr
        usernameField.typingAttributes = usernameAttr
        usernameField.attributedText = NSAttributedString(string: username, attributes: usernameAttr)
        usernameField.backgroundColor = .clear
        usernameField.textAlignment = .center
        usernameField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        usernameField.layer.cornerRadius = 4
        usernameField.layer.masksToBounds = true
        usernameField.layer.borderWidth = 1
        usernameField.layer.borderColor = UIColor.clear.cgColor
        usernameField.frame = CGRect(origin: usernameLabel.frame.origin, size: CGSize(width: usernameLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: usernameLabel.frame.height)).width, height: usernameLabel.frame.height))
        view.addSubview(usernameField)
        
        let newSize = usernameField.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 28))
        
        arrow = UIImageView(image: UIImage(systemName: "chevron.down"))
        arrow.contentMode = .scaleAspectFill
        arrow.translatesAutoresizingMaskIntoConstraints = false
        arrow.tintColor = .white
        arrow.backgroundColor = .clear
        view.insertSubview(arrow, belowSubview: usernameField)
        arrow.heightAnchor.constraint(equalToConstant: newSize.height - 2).isActive = true
        arrow.widthAnchor.constraint(equalToConstant: newSize.height - 2).isActive = true
        arrowConst = arrow.leadingAnchor.constraint(equalTo: usernameField.leadingAnchor, constant: newSize.width + 4)
        arrowConst.isActive = true
        arrow.centerYAnchor.constraint(equalTo: usernameField.centerYAnchor, constant: 2).isActive = true
        
        dismissTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dismissTap.numberOfTouchesRequired = 1
        dismissTap.numberOfTapsRequired = 1
        
        let bottomBar = UIImageView(image: UIImage(named: "bottomBar.png"))
        bottomBar.contentMode = .scaleAspectFill
        bottomBar.backgroundColor = .clear
        bottomBar.frame = CGRect(origin: CGPoint(x: 0, y: cameraView.frame.maxY - 12), size: CGSize(width: view.frame.width, height: 16))
        view.insertSubview(bottomBar, belowSubview: cameraView)

        coverView = UIView()
        coverView.backgroundColor = .clear
        coverView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(coverView, aboveSubview: cameraView)
        
        coverView.topAnchor.constraint(equalTo: cameraView.topAnchor).isActive = true
        coverView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor).isActive = true
        coverView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor).isActive = true
        coverView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor).isActive = true
        
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(CommentCell.self, forCellReuseIdentifier: "comment")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.allowsSelection = false
        tableView.allowsSelectionDuringEditing = false
        tableView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi))
        coverView.addSubview(tableView)
        
        tableView.bottomAnchor.constraint(equalTo: coverView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: coverView.leadingAnchor).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: 256).isActive = true
        tableView.widthAnchor.constraint(equalTo: coverView.widthAnchor, constant: -80).isActive = true
        
        tableViewGradient = CAGradientLayer()
        tableViewGradient.frame = CGRect(origin: CGPoint(x: 0, y: coverView.frame.maxY - 300), size: CGSize(width: coverView.frame.width - 80, height: 256))
        tableViewGradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        tableViewGradient.locations = [0,0.12]
        coverView.layer.mask = tableViewGradient
        
        noCameraView = UILabel()
        noCameraView.backgroundColor = .clear
        noCameraView.layer.cornerRadius = 4
        noCameraView.layer.masksToBounds = true
        noCameraView.translatesAutoresizingMaskIntoConstraints = false
        noCameraView.attributedText = NSAttributedString(string: "No Camera", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 27, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.white])
        noCameraView.alpha = 0
        noCameraView.isUserInteractionEnabled = true
        cameraView.addSubview(noCameraView)
        
        noCameraView.topAnchor.constraint(equalTo: cameraView.topAnchor).isActive = true
        noCameraView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor).isActive = true
        noCameraView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor).isActive = true
        noCameraView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor).isActive = true
        
        noCameraView.textAlignment = .center
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    func setupInputs() {
        frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        if frontCamera == nil || backCamera == nil {
            return
        }
        do {
            frontInput = try AVCaptureDeviceInput(device: frontCamera)
            backInput = try AVCaptureDeviceInput(device: backCamera)
            frontBack = true
        } catch {
            print("Couldn't set inputs")
        }
    }
    
    func checkPermissions(_ completion: @escaping () -> Void = {}) {
        
        if frontCamera == nil || backCamera == nil {
            noCameraView.alpha = 1
            let alert = UIAlertController(title: "No Camera Available", message: "You will not be able to use this app properly", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                completion()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let cameraAuthStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch cameraAuthStatus {
        
        case .authorized:
            if !self.firstRun {
                self.startCapture()
            }
            completion()
            return
        case .denied, .restricted:
            noCameraView.alpha = 1
            let alert = UIAlertController(title: "Note", message: "You will not be able to properly appear livë without the camera.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (action) in
                completion()
            }))
            self.present(alert, animated: true, completion: nil)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (authorized) in
                DispatchQueue.main.async {
                    if !authorized {
                        self.noCameraView.alpha = 1
                        let alert = UIAlertController(title: "Note", message: "You will not be able to properly appear livë without the camera", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            completion()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        if !self.firstRun {
                            self.startCapture()
                        }
                        completion()
                    }
                }
            })
        @unknown default:
            fatalError()
        }
        
    }
    
    func startCapture() {
        if frontInput == nil || backInput == nil { return }
        if captureSession != nil {
            self.previewLayer.removeFromSuperlayer()
            self.cameraView.layer.addSublayer(self.previewLayer)
        }
        if captureSession == nil {
            self.captureSession = AVCaptureSession()
            self.captureSession.beginConfiguration()
            self.captureSession.automaticallyConfiguresCaptureDeviceForWideColor = true
            self.captureSession.sessionPreset = .high
            self.captureSession.addInput(self.frontInput)
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            self.previewLayer.videoGravity = .resizeAspectFill
            self.previewLayer.connection?.videoOrientation = .portrait
            self.previewLayer.frame = self.cameraView.bounds
            self.cameraView.layer.addSublayer(self.previewLayer)
            self.captureSession.commitConfiguration()
            self.captureSession.startRunning()
        }
        
    }
    
    func formatViewerNumber(_ num: Int) -> String {
        var returnStr = ""
        if num < 1000 {
            returnStr = String(num)
        } else if num < 1000000 {
            let numStr = String(Float(num)/1000.0)
            let periodIndex = numStr.firstIndex(of: ".")!
            let formattedNum = numStr[numStr.startIndex...numStr.index(periodIndex, offsetBy: 1)]
            returnStr = formattedNum + "k"
        } else {
            let numStr = String(Float(num)/1000000.0)
            let periodIndex = numStr.firstIndex(of: ".")!
            let formattedNum = numStr[numStr.startIndex...numStr.index(periodIndex, offsetBy: 1)]
            returnStr = formattedNum + "m"
        }
        return returnStr
    }
    
    @objc func changeViewers() {
        if editingUsername { return }
        if viewerTimer != nil {
            viewerTimer.invalidate()
            viewerTimer = nil
        }
        if commentTimer != nil {
            commentTimer.invalidate()
            commentTimer = nil
        }
        let originalViewers = self.viewerCount!
        let originalAuto = self.autoViewers!
        viewerAlert = UIAlertController(title: "Change Viewers", message: nil, preferredStyle: .alert)
        viewerAlert.view.frame = CGRect(origin: CGPoint(x: viewerAlert.view.frame.origin.x, y: viewerAlert.view.frame.origin.y - 16), size: CGSize(width: viewerAlert.view.frame.size.width, height: viewerAlert.view.frame.size.height + 32))
        let autoButton = AlertButton()
        autoButton.alert = viewerAlert
        autoButton.layer.cornerRadius = 4
        autoButton.layer.masksToBounds = true
        autoButton.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.3)
        autoButton.setAttributedTitle(NSAttributedString(string: "\(autoViewers ? "Flux" : "Static")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .semibold), NSAttributedString.Key.foregroundColor : UIColor.label]), for: .normal)

        autoButton.translatesAutoresizingMaskIntoConstraints = false
        viewerAlert.view.addSubview(autoButton)
        autoButton.widthAnchor.constraint(equalTo: viewerAlert.view.widthAnchor, constant: -32).isActive = true
        autoButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        autoButton.centerXAnchor.constraint(equalTo: viewerAlert.view.centerXAnchor).isActive = true
        autoButton.topAnchor.constraint(equalTo: viewerAlert.view.centerYAnchor, constant: 8).isActive = true
        autoButton.bottomAnchor.constraint(equalTo: viewerAlert.view.bottomAnchor, constant: -56).isActive = true
        autoButton.addTarget(self, action: #selector(changeViewerAuto(_:)), for: .touchUpInside)
        viewerAlert.addTextField { (tf) in
            tf.addTarget(self, action: #selector(self.viewerTextFieldChanged(_:)), for: .editingChanged)
            tf.placeholder = String(originalViewers)
            tf.keyboardType = .numberPad
            tf.delegate = self
        }
        viewerAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
            self.viewerCount = originalViewers
            self.lastViewerVal = originalViewers
            self.autoViewers = originalAuto
            self.startComments()
            self.startViewers()
        }))
        let action = UIAlertAction(title: "Done", style: .default, handler: { (action) in
            self.setViewers()
            self.startViewers()
            self.startComments()
        })
        action.isEnabled = viewerAlert.textFields!.first!.text != ""
        viewerAlert.addAction(action)
        
        self.present(viewerAlert, animated: true) {
            self.viewerAlert.textFields?.first?.becomeFirstResponder()
        }
    }
    
    func startViewers() {
        if viewerTimer != nil {
            viewerTimer.invalidate()
            viewerTimer = nil
        }
        if self.viewerCount < 10 {
            autoViewers = false
        }
        if autoViewers {
            self.viewerTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(arc4random_uniform(22))/10.0 + 1.1, repeats: false, block: { (timer) in
                var nextCount = 0
                var twoThirds = arc4random_uniform(3) != 2
                while (twoThirds && self.lastViewerVal > nextCount) || nextCount == 0 {
                    var tenHundred: Bool!
                    twoThirds = arc4random_uniform(3) != 2
                    if self.viewerLabel.attributedText!.string.count == 1 {
                        tenHundred = true
                    } else {
                        tenHundred = String(self.viewerLabel.attributedText!.string[self.viewerLabel.attributedText!.string.index(self.viewerLabel.attributedText!.string.startIndex, offsetBy: 1)]) == "."
                    }
                    let divisor = self.viewerCount < 50 ? Float(arc4random_uniform(9) + 2) : Float(arc4random_uniform(tenHundred ? 48 : 51) + UInt32(tenHundred ? 3 : 50))
                    let addFloat = Float(self.viewerCount)/divisor
                    let addInt = UInt32(addFloat)
                    let addition = Float(arc4random_uniform(addInt))
                    nextCount = Int(Float(self.viewerCount) + addition)
                }
                self.lastViewerVal = nextCount
                self.setViewers(nextCount)
                self.startViewers()
            })
        }
    }
    
    @objc func changeViewerAuto(_ button: AlertButton) {
        autoViewers = !autoViewers
        button.setAttributedTitle(NSAttributedString(string: "\(autoViewers ? "Flux" : "Static")", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .semibold), NSAttributedString.Key.foregroundColor : UIColor.label]), for: .normal)
        button.alert.actions.first { (action) -> Bool in
            return action.title == "Done"
        }?.isEnabled = true
    }
    
    @objc func viewerTextFieldChanged(_ textField: UITextField) {
        if textField.text == "" {
            viewerCount = Int(textField.placeholder!)
        } else if let num = Int(textField.text!) {

            if num > 9999999 {
                viewerCount = 9999999
            } else {
                viewerCount = Int(textField.text!)
            }
            
            textField.text = String(viewerCount)
            
        } else if textField.text != "" {
            viewerCount = 0
            textField.text = String(viewerCount)
        }
        
        lastViewerVal = viewerCount
        
        viewerAlert.actions.first { (action) -> Bool in
            return action.title == "Done"
        }?.isEnabled = viewerAlert.textFields!.first!.text != "" || viewerAlert.actions.first { (action) -> Bool in
            return action.title == "Done"
        }!.isEnabled
    }
    
    func setViewers(_ newNum: Int? = nil) {
        let newViewers = newNum ?? self.viewerCount!
        
        viewerLabel.attributedText = NSAttributedString(string: formatViewerNumber(newViewers), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.black])
        
        let viewerTextFrame = viewerLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 30))
        viewerLabel.removeConstraint(viewerConst)
        viewerConst = viewerLabel.widthAnchor.constraint(equalToConstant: viewerTextFrame.width + 16)
        viewerConst.isActive = true
        
        viewerView.removeConstraint(viewerWidthConst)
        viewerWidthConst = viewerView.widthAnchor.constraint(equalTo: viewerLabel.widthAnchor, constant: 16)
        viewerWidthConst.isActive = true
        
        eyeImg.removeConstraint(eyeTrailConst)
        eyeTrailConst = eyeImg.trailingAnchor.constraint(equalTo: viewerLabel.leadingAnchor, constant: 10)
        eyeTrailConst.isActive = true
    }
    
    @objc func changePic() {
        let picSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        picSheet.addAction(UIAlertAction(title: "\(isDefaultPfp ? "Add" : "Change") Image", style: .default, handler: { (action) in
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        if !isDefaultPfp {
            picSheet.addAction(UIAlertAction(title: "Remove Image", style: .default, handler: { (action) in
                self.pfpImg = nil
                self.pfp.image = self.pfpImg
                self.isDefaultPfp = true
            }))
        }
        picSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(picSheet, animated: true, completion: nil)
    }

    @objc func flipCamera() {
        if frontInput == nil || backInput == nil || captureSession == nil || firstRun { return }
        view.isUserInteractionEnabled = false
        captureSession.beginConfiguration()
        if frontBack {
            captureSession.removeInput(frontInput)
            captureSession.addInput(backInput)
        } else {
            captureSession.removeInput(backInput)
            captureSession.addInput(frontInput)
        }
        captureSession.commitConfiguration()
        frontBack = !frontBack
        view.isUserInteractionEnabled = true
    }

}

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField != usernameField { return }
        editingUsername = true
        self.arrow.alpha = 0
        pfp.removeGestureRecognizer(picTap)
        viewerView.removeGestureRecognizer(viewerTap)
        view.removeGestureRecognizer(flipTap)
        closeButton.isUserInteractionEnabled = false
        rotateButton.isUserInteractionEnabled = false
        view.addGestureRecognizer(dismissTap)
        self.usernameField.selectedTextRange = self.usernameField.textRange(from: self.usernameField.endOfDocument, to: self.usernameField.endOfDocument)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
            textField.frame.origin = CGPoint(x: self.view.center.x - (textField.frame.width/2), y: (self.view.frame.height*0.3) - (textField.frame.height/2))
        } completion: { (complete) in
            textField.frame = CGRect(origin: CGPoint(x: 64, y: (self.view.frame.height*0.3) - 25), size: CGSize(width: self.view.frame.width - 128, height: 50))
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut) {
                textField.backgroundColor = UIColor.black
                textField.layer.borderColor = UIColor.white.cgColor
            }
            self.usernameField.selectedTextRange = self.usernameField.textRange(from: self.usernameField.endOfDocument, to: self.usernameField.endOfDocument)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let isUsernameField = textField == self.usernameField
        
        if isUsernameField {
            return true
        }
        
        let onlyNumbers = !string.contains(where: { (c) -> Bool in
            return !c.isNumber
        })
        
        if !onlyNumbers {
            return false
        }
                
        if Int(textField.text! + string)! > 9999999 {
            if string.count > 1 || range.length > 1 {
                return true
            }
            return false
        }
                
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let currentText = textField.attributedText!.string
        var newText = currentText.lowercased()
        let currentSelection = textField.selectedTextRange!.start
        
        if currentText.count > 15 {
            newText = String(currentText[currentText.startIndex...currentText.index(currentText.startIndex, offsetBy: 14)]).lowercased()
        }
        
        newText = newText.replacingOccurrences(of: " ", with: "_")
        newText = newText.replacingOccurrences(of: "\n", with: "_")
        newText = newText.replacingOccurrences(of: "\r", with: "_")
        
        textField.attributedText = NSAttributedString(string: newText, attributes: usernameAttr)
        
        username = textField.attributedText?.string.lowercased()
        
        if newText == "" { return }
        usernameField.attributedText = NSAttributedString(string: username, attributes: usernameAttr)
        textField.selectedTextRange = textField.textRange(from: currentSelection, to: currentSelection)
        
        let newSize = usernameField.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 28))
        arrowConst.constant = newSize.width + 4
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == usernameField && textField.text == "" {
            textField.text = username
            textField.attributedText = NSAttributedString(string: username, attributes: usernameAttr)
            textField.selectedTextRange = textField.textRange(from: textField.selectedTextRange!.start, to: textField.selectedTextRange!.start)
            
            let newSize = usernameField.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 28))
            arrowConst.constant = newSize.width + 4
        }
                
        if textField != usernameField {
            editingUsername = false
            return
        }
        
        view.removeGestureRecognizer(dismissTap)
        
        usernameLabel.attributedText = NSAttributedString(string: username, attributes: usernameAttr)
                        
        let newSize = CGSize(width: self.usernameLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.usernameLabel.frame.height)).width, height: self.usernameLabel.frame.height)
        
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut) {
            textField.backgroundColor = .clear
            textField.layer.borderColor = UIColor.clear.cgColor
        } completion: { (complete) in
            
            textField.frame = CGRect(origin: CGPoint(x: self.view.center.x - (newSize.width/2), y: (self.view.frame.height*0.3) - (newSize.height/2)), size: newSize)
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
                textField.frame.origin = CGPoint(x: self.usernameLabel.frame.minX, y: self.usernameLabel.center.y - (newSize.height/2))
            } completion: { (complete) in
                self.editingUsername = false
                self.arrow.alpha = 1
                self.addTapRecognizers()
                self.closeButton.isUserInteractionEnabled = true
                self.rotateButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return false
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pfpImg = img
            self.pfp.image = pfpImg
            isDefaultPfp = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Calculate size of attributed text from both user and text views per cell
        var calculatedHeight: CGFloat = 0
        
        let comment = comments[indexPath.row]
        let tempCell = CommentCell()
        tempCell.configure(comments[indexPath.row], self)
        
        if comment.type == .commented {
            calculatedHeight += tempCell.userTextView.sizeThatFits(CGSize(width: view.frame.width - 130, height: CGFloat.greatestFiniteMagnitude)).height
            calculatedHeight += tempCell.commentTextView.sizeThatFits(CGSize(width: view.frame.width - 130, height: CGFloat.greatestFiniteMagnitude)).height
            
            calculatedHeight += 4
        } else {
            calculatedHeight += tempCell.userTextView.sizeThatFits(CGSize(width: view.frame.width - 130, height: CGFloat.greatestFiniteMagnitude)).height
        }
        calculatedHeight += 16
        return max(calculatedHeight, 50)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comment") as! CommentCell
        
        let comment = comments[indexPath.row]
        cell.configure(comment, self)
        
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        cell.layoutIfNeeded()
        return cell
    }

}

extension ViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return !touch.view!.isKind(of: UIButton.self) && touch.view != self.usernameField && touch.view != self.usernameLabel && touch.view != self.viewerView && touch.view != pfp
    }
    
}

class AlertButton: UIButton {
    
    var alert: UIAlertController!
    
}

class CommentCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "comment")
        self.backgroundColor = .clear
        self.textLabel?.removeFromSuperview()
        self.detailTextLabel?.removeFromSuperview()
        self.selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        userTextView?.centerVertically()
        commentTextView?.centerVertically()
    }
            
    var pfp: UIImageView!
    var badgeView: UIImageView!
    var newContent: UIView!
    var userTextView: UITextView!
    var commentTextView: UITextView!
    var comment: Comment!
    var userHeight: NSLayoutConstraint!
    var commentHeight: NSLayoutConstraint!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.userTextView.text = nil
        self.userTextView.attributedText = nil
        self.userTextView.removeConstraints(self.userTextView.constraints)
        self.userTextView.removeFromSuperview()
        
        self.commentTextView?.text = nil
        self.commentTextView?.attributedText = nil
        if self.commentTextView != nil {
            self.commentTextView?.removeConstraints(self.commentTextView.constraints)
        }
        self.commentTextView?.removeFromSuperview()
        
        self.pfp.image = nil
        
        self.badgeView?.removeFromSuperview()
        self.badgeView = nil
    }
    
    func configure(_ comment: Comment,_ vc: ViewController) {
        self.comment = comment
        
        newContent = UIView()
        newContent.backgroundColor = UIColor.systemGray.withAlphaComponent(0.3)
        newContent.backgroundColor = .clear
        newContent.layer.masksToBounds = false
        newContent.translatesAutoresizingMaskIntoConstraints = false
        newContent.isUserInteractionEnabled = false
        contentView.addSubview(newContent)
        
        newContent.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 8).isActive = true
        newContent.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -8).isActive = true
        newContent.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 8).isActive = true
        newContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -8).isActive = true
        
        pfp = UIImageView(image: comment.pfp)
        pfp.contentMode = .scaleAspectFill
        pfp.translatesAutoresizingMaskIntoConstraints = false
        pfp.backgroundColor = .clear
        pfp.layer.cornerRadius = 17
        pfp.layer.masksToBounds = true
        newContent.addSubview(pfp)
        
        pfp.widthAnchor.constraint(equalToConstant: 34).isActive = true
        pfp.heightAnchor.constraint(equalToConstant: 34).isActive = true
        pfp.leadingAnchor.constraint(equalTo: newContent.leadingAnchor).isActive = true
        pfp.topAnchor.constraint(equalTo: newContent.topAnchor).isActive = true
        
        if comment.type == .commented {
            userTextView = UITextView()
            userTextView.attributedText = comment.user
            userTextView.translatesAutoresizingMaskIntoConstraints = false
            userTextView.textContainerInset = .zero
            newContent.addSubview(userTextView)
            
            userTextView.leadingAnchor.constraint(equalTo: pfp.trailingAnchor, constant: 4).isActive = true
            userTextView.trailingAnchor.constraint(equalTo: newContent.trailingAnchor).isActive = true
            userTextView.topAnchor.constraint(equalTo: newContent.topAnchor).isActive = true
            userHeight = userTextView.heightAnchor.constraint(equalToConstant: userTextView.sizeThatFits(CGSize(width: newContent.frame.width - (pfp.frame.width + 8), height: CGFloat.greatestFiniteMagnitude)).height)
            userHeight.isActive = true
            
            commentTextView = UITextView()
            commentTextView.attributedText = comment.text
            commentTextView.translatesAutoresizingMaskIntoConstraints = false
            commentTextView.textContainerInset = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            newContent.addSubview(commentTextView)
            
            commentTextView.leadingAnchor.constraint(equalTo: pfp.trailingAnchor, constant: 4).isActive = true
            commentTextView.trailingAnchor.constraint(equalTo: newContent.trailingAnchor).isActive = true
            commentTextView.topAnchor.constraint(equalTo: userTextView.bottomAnchor).isActive = true
            commentHeight = commentTextView.heightAnchor.constraint(equalToConstant: commentTextView.sizeThatFits(CGSize(width: newContent.frame.width - (pfp.frame.width + 8), height: CGFloat.greatestFiniteMagnitude)).height)
            commentHeight.isActive = true
            
        } else {
            userTextView = UITextView()
            userTextView.attributedText = comment.user
            userTextView.translatesAutoresizingMaskIntoConstraints = false
            userTextView.textContainerInset = .zero
            newContent.addSubview(userTextView)
            
            userTextView.leadingAnchor.constraint(equalTo: pfp.trailingAnchor, constant: 4).isActive = true
            userTextView.trailingAnchor.constraint(equalTo: newContent.trailingAnchor).isActive = true
            userTextView.topAnchor.constraint(equalTo: newContent.topAnchor).isActive = true
            userTextView.bottomAnchor.constraint(equalTo: newContent.bottomAnchor).isActive = true
        }
                
        userTextView.backgroundColor = .clear
        commentTextView?.backgroundColor = .clear

        if comment.verified {
            badgeView = UIImageView(image: UIImage(named: "badge.png"))
            badgeView.contentMode = .scaleAspectFill
            badgeView.translatesAutoresizingMaskIntoConstraints = false
            badgeView.backgroundColor = .clear
            badgeView.layer.cornerRadius = 8
            badgeView.layer.masksToBounds = true
            newContent.addSubview(badgeView)
            
            badgeView.widthAnchor.constraint(equalToConstant: 16).isActive = true
            badgeView.heightAnchor.constraint(equalToConstant: 16).isActive = true
            let width = userTextView.sizeThatFits(CGSize(width: vc.view.frame.width - 138, height: CGFloat.greatestFiniteMagnitude)).width
            badgeView.leadingAnchor.constraint(equalTo: userTextView.leadingAnchor, constant: width).isActive = true
            badgeView.centerYAnchor.constraint(equalTo: userTextView.centerYAnchor).isActive = true
        }
        
        setNeedsLayout()
        layoutIfNeeded()
        
        userHeight?.constant = userTextView.sizeThatFits(CGSize(width: newContent.frame.width - (pfp.frame.width + 8), height: CGFloat.greatestFiniteMagnitude)).height
        commentHeight?.constant = commentTextView.sizeThatFits(CGSize(width: newContent.frame.width - (pfp.frame.width + 8), height: CGFloat.greatestFiniteMagnitude)).height
        
        userTextView.centerVertically()
        commentTextView?.centerVertically()
    }
    
}

class Comment: NSObject {
    
    var text: NSAttributedString!
    var user: NSAttributedString!
    var pfp: UIImage!
    var verified: Bool!
    var type: CommentType!
    var vc: ViewController!
    
    private var userAttr = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedString.Key.foregroundColor : UIColor.white]
    private var textAttr = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.white]
    
    init(_ type: CommentType,_ extra: Int = 0,_ vc: ViewController) {
        super.init()
        self.vc = vc
        self.type = type
        self.pfp = generatePfp()
        self.verified = false
        
        switch type {
        
        case .commented:
            user = NSAttributedString(string: generateUser(), attributes: userAttr)
            text = NSAttributedString(string: generateText(), attributes: textAttr)
            self.verified = arc4random_uniform(12) == 0
        case .joinSingle:
            user = NSAttributedString(string: generateUser() + " joined", attributes: userAttr)
            text = nil
        case .joinMultiple:
            var first = ""
            var second = ""
            
            while first == second {
                first = generateUser()
                second = generateUser()
            }
            
            user = NSAttributedString(string: "\(first), \(second) and \(String(extra)) \(extra == 1 ? "other" : "others") joined", attributes: userAttr)
            text = nil
        }
        
    }
    
    func generateUser() -> String {
        var rand = vc.usernamesList[Int(arc4random_uniform(UInt32(vc.usernamesList.count)))]
        while vc.lastFiveUsers.contains(rand) {
            rand = vc.usernamesList[Int(arc4random_uniform(UInt32(vc.usernamesList.count)))]
        }
        if vc.lastFiveUsers.count >= 5 {
            vc.lastFiveUsers.removeFirst()
        }
        vc.lastFiveUsers.append(rand)
        return rand
    }
    
    func generateText() -> String {
        var rand = vc.commentsList[Int(arc4random_uniform(UInt32(vc.commentsList.count)))]
        while vc.lastFiveComments.contains(rand) {
            rand = vc.commentsList[Int(arc4random_uniform(UInt32(vc.commentsList.count)))]
        }
        if vc.lastFiveComments.count >= 5 {
            vc.lastFiveComments.removeFirst()
        }
        vc.lastFiveComments.append(rand)
        return rand.replacingOccurrences(of: "@", with: vc.usernameField.attributedText!.string)
    }
    
    func generatePfp() -> UIImage {
        var rand = Int(arc4random_uniform(UInt32(vc.pfpList.count)))
        while vc.lastFivePfp.contains(rand) {
            rand = Int(arc4random_uniform(UInt32(vc.pfpList.count)))
        }
        if vc.lastFivePfp.count >= 5 {
            vc.lastFivePfp.removeFirst()
        }
        vc.lastFivePfp.append(rand)
        return vc.pfpList[rand]
    }
    
}

enum CommentType {
    case joinSingle
    case joinMultiple
    case commented
}

extension UITextView {
    func centerVertically(_ frame: CGRect? = nil, _ completion: @escaping () -> Void = {}) {
        let calcFrame = frame ?? self.bounds
        var topCorrect = (calcFrame.size.height - self.contentSize.height * self.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect
        self.contentInset.top = topCorrect
        completion()
    }
}

extension UIFont {

    func withTraits(_ traits: UIFontDescriptor.SymbolicTraits) -> UIFont {

        // create a new font descriptor with the given traits
        guard let fd = fontDescriptor.withSymbolicTraits(traits) else {
            // the given traits couldn't be applied, return self
            return self
        }
            
        // return a new font with the created font descriptor
        return UIFont(descriptor: fd, size: pointSize)
    }

    func italics() -> UIFont {
        return withTraits(.traitItalic)
    }

    func bold() -> UIFont {
        return withTraits(.traitBold)
    }

    func boldItalics() -> UIFont {
        return withTraits([ .traitBold, .traitItalic ])
    }
}
